import React, { useState } from 'react';
import FormImages from './FormImages';
import FormSuccess from './FormSuccess';


export default function Images() {
  const [isSubmitted, setIsSubmitted] = useState(false);
  function submitForm() {
    setIsSubmitted(true);
  }
  return (
    <div className="imagesIpfs">
      <div className="container">
      <div className="background">
          
        </div>
        <div className="content">      
          <div className='form-content'>
            {!isSubmitted ? (
              <FormImages submitForm={submitForm} />
            ) : (
              <FormSuccess />
            )}
          </div>
        </div>
 
      </div>   
    </div>
    
  );
}

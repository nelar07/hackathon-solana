import React from "react";
import Images from "./ImagesIpfs/Images";
import NFT from "./NFTIpfs/NFT";
import Signup from "./Signup";

export default function Ipfs() {
  return (
    <div className="ipfs">
          
            <div className="container">
              <div className="content">
                <h1 className="title">IPFS</h1>
                <p className="description">
                Register with moralis and your wallet. Upload files and generate your NFTs.
                </p>
        
              </div>
            
            </div>
          
      
          <div className="container">
                <div className="content">
                    <Signup/>          
                  </div>
                  <div className="content">  
                    <Images/>  
                  </div>
                  <div className="content">  
                    <NFT/>  
                  </div>
          </div>
    </div>
  );
}


import React, { useState } from 'react';
import FormNFT from './FormNFT';
import FormSuccess from './FormSuccess';


export default function NFT() {
  const [isSubmitted, setIsSubmitted] = useState(false);
  function submitForm() {
    setIsSubmitted(true);
  }
  return (
    <div className="NFTIpfs">
      <div className="container">
        <div className="background">
           
        </div>
        <div className="content">      
          <div className='form-content'>
            {!isSubmitted ? (
              <FormNFT submitForm={submitForm} />
            ) : (
              <FormSuccess />
            )}
          </div>
        </div>
 
      </div>   
    </div>
    
  );
}

export default function validateInfo(values) {
  let errors = {};

  if (!values.name.trim()) {
    errors.name = 'Name required';
  }
  if (!values.description) {
    errors.description = 'Description is required';
  } else if (values.description.length < 20) {
    errors.description = 'Description needs to be 20 characters or more';
  }
  if (!values.price.trim()) {
    errors.price = 'Price required';
  }

  return errors;
}

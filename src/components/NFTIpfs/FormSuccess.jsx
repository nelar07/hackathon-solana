import React from 'react';
import validate from './validateInfo';
import useForm from './useForm';

const FormSuccess = ({ submitForm }) => {
  const { handleChange, handleSubmit, values, errors } = useForm(
    submitForm,
    validate
  );

  return (
    <div className="NFTIpfs">
      <div className="container">
        <div className="content">
        
      <form onSubmit={handleSubmit} className='form' noValidate>
        <p className="title">
        Upload ipfs NFTs
          </p>
        <div className='form-inputs'>
          <label className='form-label'>Name</label>
          <input
            className='form-input'
            type='text'
            name='name'
            placeholder='Enter name'
            value={values.name}
            onChange={handleChange}
          />
          {errors.name && <p>{errors.name}</p>}
        </div>
        <div className='form-inputs'>
          <label className='form-label'>Description</label>
          <input
            className='form-input'
            type='text'
            name='description'
            placeholder='Enter description'
            value={values.description}
            onChange={handleChange}
          />
          {errors.description && <p>{errors.description}</p>}
        </div>
        <div className='form-inputs'>
          <label className='form-label'>Price</label>
          <input
            className='form-input'
            type='text'
            name='price'
            placeholder='Enter price'
            value={values.price}
            onChange={handleChange}
          />
          {errors.price && <p>{errors.price}</p>}
        </div>
   
        <button className='form-input-btn' type=''>
        Search 
        </button>
        <button className='form-input-btn' type='submit'>
        Upload 
        </button>
        <button className='form-input-btn' type=''>
        Generate NFT 
        </button>
      </form>
    
        </div>
      </div>
    </div>
  );
 
};

export default FormSuccess;

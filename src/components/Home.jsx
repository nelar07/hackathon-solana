import React from "react";
import home from "../assets/home.png";
export default function Home() {
  return (
    <div className="home">
      <div className="container">
        <div className="content">
          <p className="sub-title">NFTs with Solana</p>
          <h1 className="title">Campus project</h1>
          <p className="description">
          This platform allows the creation of mixed reality experiences, which can be viewed through smart glasses, 
          these experiences are created from our campus project where the user with a basic use of unity can create
           these experiences and upload them to our platform using ipfs, 
          to later be able to generate the nft, then sell it through our market, 
          built on the <span>Solana</span> blockchain and Moralis.
          </p>
          <button>Login</button>
        </div>
        <div className="image-container">
          <div className="image">
            <img src={home} alt="home image" />
          </div>
          <div className="ellipse-container">
            
          </div>
        </div>
      </div>
    </div>
  );
}
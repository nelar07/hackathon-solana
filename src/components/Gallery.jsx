import React from "react";
import Card from "./Card";
import heart from "../assets/heart.png";
import skeleton from "../assets/skeleton.png";
import brain from "../assets/brain.png";
import eye from "../assets/eye.png";
import bomba from "../assets/bomba.png";
import compresor from "../assets/compresor.png";
import triaxial1 from "../assets/triaxial1.png";
import triaxial2 from "../assets/triaxial2.png";

export default function SuperRare() {
  const data = [
    {
      image: heart,
      title: "Heart",
      price: 2.99,
      description: "Heart with mixed reality",
      
    },
    {
      image: skeleton,
      title: "Skeleton",
      price: 2.99,
      description: "Skeleton with mixed reality",
      
    },
    {
      image: brain,
      title: "Brain",
      price: 2.99,
      description: "Brain with mixed reality",
      
    },
    {
      image: eye,
      title: "Eye",
      price: 2.99,
      description: "Eye with mixed reality",
      
    },
    {
      image: triaxial1,
      title: "Triaxial Machine",
      price: 2.99,
      description: "Triaxial machine for earth testing",
      
    },
    {
      image: bomba,
      title: "Water compressor",
      price: 2.99,
      description: "Water compressor with mixed reality",
      
    },
    {
      image: compresor,
      title: "Water compressor",
      price: 2.99,
      description: "Water compressor with mixed reality",
      
    },
    {
      image: triaxial2,
      title: "Triaxial machine",
      price: 2.99,
      description: "Triaxial machine for earth testing",
      
    },
   
  ];
  return (
    <div className="gallery">
      <div className="title-container">
        <h2 className="title">Our NFTs gallery</h2>
        <p className="description">
         Explore our NTTs gallery
        </p>
      </div>
      <div className="cards">
        {data.map(({ image, title, price, description}, index) => (
          <Card
            image={image}
            title={title}
            price={price}
            description={description}
            key={index}
          />
        ))}
      </div>
    </div>
  );
}

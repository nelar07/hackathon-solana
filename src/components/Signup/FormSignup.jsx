import React from 'react';
import validate from './validateInfo';
import useForm from './useForm';
import {useState} from 'react';
import { useMoralis } from "react-moralis";
import { async } from 'q';

const FormSignup = ({ submitForm }) => {
  const { 
    authenticate, 
    isAuthenticated, 
    user, 
    logout, 
    setUserData,
    isInitializing,
    isInitialized,
    isAuthenticating,
  } = useMoralis();
  const [username, setUsername] = useState();
  const [password, setPassword] = useState();
  const [email, setEmail] = useState();
  const { handleChange, handleSubmit, values, errors } = useForm(
    submitForm,
    validate
  );


  return (
    <div className="signup">
      <div className="container">
        <div className="content">
        
      <form onSubmit={handleSubmit} className='form' noValidate>
        <h1>
        Register with moralis and your wallet
        </h1>
        
        <div className='form-inputs'>
          <label className='form-label'>Username</label>
          <input
            className='form-input'
            type='text'
            name='username'
            placeholder='Enter your username'
            value={values.username}
            onChange={handleChange}
          />
          {errors.username && <p>{errors.username}</p>}
        </div>
        <div className='form-inputs'>
          <label className='form-label'>Email</label>
          <input
            className='form-input'
            type='email'
            name='email'
            placeholder='Enter your email'
            value={values.email}
            onChange={handleChange}
          />
          {errors.email && <p>{errors.email}</p>}
        </div>
        <div className='form-inputs'>
          <label className='form-label'>Password</label>
          <input
            className='form-input'
            type='password'
            name='password'
            placeholder='Enter your password'
            value={values.password}
            onChange={handleChange}
          />
          {errors.password && <p>{errors.password}</p>}
        </div>
     
        <button className='form-input-btn' type='submit' onClick={ async () => {
              await authenticate({
                type: "sol",
              });
              await setUserData({
                username: values.username,
                email: values.email,
                password: values.password
              })
              console.log('============');
            }}>
          Sign up
        </button>
        <button className='form-input-btn' type='submit'>
          Login
        </button>        
        <button className='form-input-btn' type='submit'onClick={() => {
              logout();
            }}>
          Logout
        </button>
        <p>{ isAuthenticated ? user.get("solAddress") : ""}</p>

      </form>
    
        </div>
      </div>
    </div>
  );
 
};

export default FormSignup;

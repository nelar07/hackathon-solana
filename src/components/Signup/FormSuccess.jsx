import React from 'react';
import validate from './validateInfo';
import useForm from './useForm';

const FormSuccess = ({ submitForm }) => {
  const { handleChange, handleSubmit, values, errors } = useForm(
    submitForm,
    validate
  );

  return (
    <div className="signup">
      <div className="container">
        <div className="content">
        
      <form onSubmit={handleSubmit} className='form' noValidate>
        <h1>
        Register with moralis and your wallet
        </h1>
        <div className='form-inputs'>
          <label className='form-label'>Username</label>
          <input
            className='form-input'
            type='text'
            name='username'
            placeholder='Enter your username'
            value={values.username}
            onChange={handleChange}
          />
          {errors.username && <p>{errors.username}</p>}
        </div>
        <div className='form-inputs'>
          <label className='form-label'>Email</label>
          <input
            className='form-input'
            type='email'
            name='email'
            placeholder='Enter your email'
            value={values.email}
            onChange={handleChange}
          />
          {errors.email && <p>{errors.email}</p>}
        </div>
        <div className='form-inputs'>
          <label className='form-label'>Password</label>
          <input
            className='form-input'
            type='password'
            name='password'
            placeholder='Enter your password'
            value={values.password}
            onChange={handleChange}
          />
          {errors.password && <p>{errors.password}</p>}
        </div>
     
        <button className='form-input-btn' type='submit'>
          Sign up
        </button>
        
      </form>
    
        </div>
      </div>
    </div>
  );
 
};

export default FormSuccess;

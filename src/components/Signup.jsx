import React, { useState } from 'react';
import FormSignup from './Signup/FormSignup';
import FormSuccess from './Signup/FormSuccess';


export default function Signup() {
  const [isSubmitted, setIsSubmitted] = useState(false);
  function submitForm() {
    setIsSubmitted(true);
  }
  return (
    <div className="signup">
      <div className="container">
        <div className="background">
        </div>
        <div className="content">      
          <div className='form'>
            {!isSubmitted ? (
              <FormSignup submitForm={submitForm} />
            ) : (
              <FormSuccess />
            )}
          </div>
        </div>
      </div>   
    </div>
  );
}

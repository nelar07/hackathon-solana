import React from "react";
import SOL from "../assets/SOL.png";
export default function Card({ image, description, title, price}) {
  return (
    <div className="card">
      <div className="card-image">
        <img src={image} alt="" />
      </div>
      <div className="card-content">
        <div className="card-heading">
          <span className="card-top">Mixed Reality</span>
        </div>
        <div className="card-details">
          <h4 className="card-title">{title}</h4>
          <div className="card-price">
            <img src={SOL} alt="super eth" />
            <h4>{price} SOL</h4>
          </div>
        </div>
        <div className="card-sub-details">
          <span className="card-series">{description}</span>
        </div>
      </div>
    </div>
  );
}
import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import { MoralisProvider } from "react-moralis";

const appId = "bLAE1jAz2ktOEDzXpja1ddSOGfuKXU8nOJD7mB60";
const serverUrl = "https://bikkg6oplbvs.usemoralis.com:2053/server";

ReactDOM.render(
  <React.StrictMode>
    <MoralisProvider appId={appId} serverUrl={serverUrl}>
      <App />
    </MoralisProvider>
  </React.StrictMode>,
  document.getElementById("root")
);
